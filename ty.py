# ______________EXERCICE 1

# # Ecrire un programme qui affiche 'Coucou les simploniens'
print('Coucou les simploniens')

# # Ecrire un programme qui permet de saisir le nom de l'utilisateur et de renvoyer 'Bonjour' suivie du nom
nom = input('entrez votre nom')
print("Bonjour ", nom, "!")

# # Ecrire un pro. qui demande a l'utilisateur 2 nombre et afficher la somme 
a = int(input())
b = int(input())
c = a + b
print( c )

# Ecrire unn pro. qui affiche une suite de 12 nbr dont chaque terme soit egal au triple du terme précédent
triple = 3
i=1
result = i * triple
for i in range(0, 12):
   result *= 3
   print (result*triple)

# recréer les symboles
for i in range(8) :
    print( i * '*')

# ___________EXERCICE 2

# Constituez une liste semaine contenant les 7 jours de la semaine. À partir de cette liste, comment récupérez-vous seulement les 5 premiers jours
#  de la semaine d’une part, et ceux du week-end d’autre part 
#  Cherchez un autre moyen pour arriver au même .

semaine = ['lundi','mardi','mercredi','jeudi','vendredi','samedi','dimanche']
# 1 façon : 
print(semaine[0:5])
print(semaine[5:7])
# # 2 façon : 
print(semaine[-7:-2])
print( semaine[-2:])

# Inversez les jours de la semaine en une commande.
 print(semaine[:: -1])

# Créez 4 listes hiver, printemps, ete et automne contenant les mois correspondant à ces saisons. Créez ensuite une liste saisons contenant 
# les sous-listes hiver, printemps,ete et automne. Prévoyez ce que valent les variables suivantes, puis vérifiez-le dans l’interpréteur :
# saisons[2]
# saisons[1][0]
# saisons[1:2]
# saisons[:][1]


hiver = ['decembre', 'janvier', 'fevrier']
printemps = ['mars', "avril", 'mai']
ete = [ 'juin', 'juillet', 'aout']
automne = ['septembre', 'octobre', 'novembre']

saisons = [[hiver], [printemps],[ete], [automne]]
print(saisons[2])
print(saisons[1][0])
print(saisons[1:2])
print(saisons[:][1])

# Comment expliquez-vous ce dernier résultat ?
#  ----> Comprend pas la question , a vous de faire des consignes claire ( c'est pas faute de le répéter ! )