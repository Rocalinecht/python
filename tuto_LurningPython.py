# x = 1
# if x ==1:
#     print(x)

# myint = 7
# print(myint)

# myfloat = 7.0
# print(myfloat)
# myfloat = float(7)
# print(myfloat)

# mystring = "hello"
# print(mystring)
# mystring = 'hello'
# print(mystring)

# newstring = "Don't worry about the apostrophe"
# print(newstring)

# one = 1
# two = 2
# three = one + two
# print(three)

# hello = 'hello'
# world = 'world'
# helloworld = hello +' ' + world
# print(helloworld)

# a,b = 3,4
# print(a,b)

# mystring = "hello"
# myfloat = 10.0
# myint = 20

# if mystring == "hello" :
#     print("String : %s" % mystring)
# if isinstance(myfloat,float) and myfloat ==10.0 :
#     print("Float : %f" %myfloat)
# if isinstance(myint,int) and myint == 20 :
#     print("Integer : %d" % myint)

# _____________________LIST
# mylist = []
# mylist.append(1)
# mylist.append(2)
# mylist.append(3)
# print(mylist[0])
# print(mylist[1])
# print(mylist[2])

# for x in mylist:
#     print(x)

# newlist = [4, 5, 6]
# print(newlist)

# numbers = [1, 2, 3]
# string = ['hello']
# names = ['John', 'Eric', 'Jessica']
# seconde_name = names[1]

# print(numbers)
# print(string)
# print('The second name on the list is %s' %seconde_name)

# ______________________ARITHMETIC OPERATOR
# number = 1 + 2 * 3 / 4.0
# print(number)

# remainder = 11%3
# print(remainder)

# squared = 7 ** 2
# cubed = 2**3
# print(squared)
# print(cubed)

# _____________________OPERATOR WITH STRING

# lotofhello = 'hello ' * 10
# print(lotofhello)

# _____________________OPERATOR WITH LIST
# event_numbers = [2,4,6,8]
# odd_nubers = [1,3,5,7]
# all_numbers = odd_nubers + event_numbers
# print(all_numbers)

# print([1,2,3] * 3)
# --> affiche 3fois de suite le tableau 

# ____EXO
# x = object()
# y = object()

# x_list = [x] *10
# y_list = [y] * 10
# big_list = x_list + y_list

# print("x_list contains %d object" %len(x_list))
# print("y_list contains %d object" %len(y_list))
# print("big_list contains %d object" %len(big_list))

# if x_list.count(x) == 10 and y_list.count(y) == 10:
#     print("Alomost there .. ")
# if big_list.count(x) == 10 and big_list.count(y) == 10:
#     print("Great")

# _____________________________STRING FORMATTING
# name = 'John'
# age = 23
# print('%s is %d years old' % (name,age))

# # NEED TO KNOW : 
# #           - %s = string
# #           - %d = integers
# #           - %f = floating point number

# mylist = [1,2,3]
# print("A list : %s" %mylist)

# # __EXO
# data = ("John", "Doe", 53.44)
# format_string = 'Hello %s %s , Your current balance is %s $'
# print(format_string  % data)

# _______________________________BASIC STRING OPERATIONS
astring = "Hello world!"
print("single quotes are ''")
print(len(astring))
# --> calcule le nombre de caractères , espace compris

print(astring.index("o"))
# ---> indique le numero d'index ou se trouve la lettre

print(astring.count("l"))
# ---> conte le nombre de fois qu'il y a la lettre L

print(astring[3:7])
# -->   affiche les caractère compris dans l'index 3 à 7

print(astring[:: -1])
# --> inverse les caratères
