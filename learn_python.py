# Adapter le code précédent pour afficher tous les nombres de 1 à 100.
for i in range(101):
    print(i)

# Afficher un rectangle de taille 4x5 composé de "x".
for i in range(4) :
    i = 'x'
    print( i * 5 )

# Afficher tous les nombres de à 100 à 1.
cent = 100
for loop in range(100):
   print(cent)
   cent = cent - 1

# Afficher un triangle composé de 10 "o" au total.
for i in range(10) :
    print( i * 'o')

# Écrire un programme qui demande 5 prénoms, les stocke dans un tableau, puis les affiche.
prenom1 = input('entrez un prénom');
prenom2 = input('entrez un 2e prénom');
prenom3 = input('entrez un 3e prénom');
prenom4 = input('entrez un 4e prénom');
prenom5 = input('entrez un 5e prénom');

list_prenom = []
list_prenom.append(prenom1)
list_prenom.append(prenom2)
list_prenom.append(prenom3)
list_prenom.append(prenom4)
list_prenom.append(prenom5)

print(list_prenom)


# Compléter ce code pour afficher les films diffusés après 12h :

horaires = [
    {'film': 'Seul sur Mars', 'heure': 9},
    {'film': 'Astérix et Obélix Mission Cléopâtre', 'heure': 10},
    {'film': 'Star Wars VII', 'heure': 15},
    {'film': 'Time Lapse', 'heure': 18},
    {'film': 'Fatal', 'heure': 20},
    {'film': 'Limitless', 'heure': 20},
]

for i in range(len(horaires)) : 
    heures = horaires[i]['heure']
    if (heures > 12) :
        print(horaires[i]['film'])